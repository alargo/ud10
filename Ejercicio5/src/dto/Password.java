package dto;

import java.util.Random;

public class Password {
	
	protected int longitud;
	protected String contraseña;

	protected final int longitudF = 8;
	
	//creamos las variables y una que va a tener un valor por defecto, se lo asignamos a uno de los constructores
	public Password() {
		this.longitud = longitudF;
		this.contraseña = "";
	}
	
	
	public Password(int longitud) {
		this.longitud = longitud;
		this.contraseña = generarPassword();
	}
	
	//getters y setters
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	public String getContraseña() {
		return contraseña;
	}
	
	//generamos la contraseña con un char que nos va a almacenar las letras y numeros para despues mediante un bucle ir sacando cada una de las letras
	public String generarPassword() {
		
		char caracter [] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

		Random random = new Random();

		int c = 0;
		
		String contra = "";
		
		while(c<this.longitud) {	
			if(c % 2 != 0) {
				int numero = random.nextInt(5);
				contra = contra + numero;			
			}else {
				int letra = random.nextInt(caracter.length);	
				char posicion = caracter[letra];
				contra = contra + posicion;
			}

			c++;
		}
		
		return contra;
	}

	
	
	//creamos variables para almacenar cuando salga cada letra, despues con un bucle ver si cumple los parametros para que sea fuerte o no la contraseña
	
	public boolean esFuerte() {
	
		int minus = 0;
		int mayus = 0;
		int c = 0;
		
		boolean comprobar = false;
		
		String[] letra = contraseña.split("");
		for (int i = 0; i < letra.length; i++) {
			char caracter = letra[i].charAt(0);
			
			if(Character.isUpperCase(caracter)) {
				mayus++;		
			}
			if(Character.isLowerCase(caracter)) {
				minus++;			
			}
			if(Character.isDigit(caracter)) {
				c++;
				
			}
		}
		
		if(c >= 5 && mayus >= 2 && minus >= 1 ) {
			comprobar = true;
		}
		
		return comprobar;
	}
	
	
}
