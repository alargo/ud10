import java.util.Scanner;

import dto.Password;
import exceptions.ComprobarL;

public class mainApp {

	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(System.in);	
	        System.out.print("Cuantas contraseñas quieres: ");
	        int TamañoA = sc.nextInt();
	        System.out.print("De que tamaño quieres las contraseñas: ");
	        int TamañoC = sc.nextInt();
	        //se crean variables para saber la longitud y cuantas contraseñas se quiere
	        
	        
	        //se controla que sean minimo de 12 caracteres si no saltara el error y hacemos dos bucles para ir contando el tamaño y 
	        //comprobar si la contraseña que devuelve cumple con los parametros de una contraseña fuerte
			if(TamañoC < 12) {
				throw new ComprobarL(404); 
			}else {
				
				Password[] passwords = new Password[TamañoA];
				
				for (int i = 0; i < passwords.length; i++) {
					passwords[i] = new Password(TamañoC);
				}
				
				boolean[] contraseñaFuerte = new boolean[TamañoA];
				
				for (int i = 0; i < contraseñaFuerte.length; i++) {
					contraseñaFuerte[i] = passwords[i].esFuerte();
				}
				for (int i = 0; i < contraseñaFuerte.length; i++) {
					System.out.println("Contrasea: "+passwords[i].getContraseña()+" Valor: "+contraseñaFuerte[i]);
				}
			}
			
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}


	}

}
