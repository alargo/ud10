package exception;

public class miException extends Exception {

	private int codigoException;

	public miException(int codigoError) {
		super();
		this.codigoException = codigoError;
	}
	
	public miException() {
		super();
	}
	
	//hacemos el metodo y los casos para mostrar si son par o impar

	@Override
	public String getMessage() {
		String mensaje="";
		switch (codigoException) {
		case 0:
			mensaje="Es par";
			break;
		case 1:
			mensaje="Es impar";
			break;
		default:
			break;
		}
		
		return mensaje;
		
	}
	
	
	
}
