import exception.miException;

public class mainApp {

	public static void main(String[] args) {
		int num;
		
		try {
			num = (int) ((Math.random() * 500)+1);
			//generamos un numero y dependiendo del que salga comprobamos si es par o impar
			
			System.out.println("El numero es:" + num);
			
			if (num%2 != 1) { throw new miException(0);}
			else{ throw new miException(1);}
			
		} catch (miException e) {
			System.out.println(e.getMessage());
		}

	}

}
